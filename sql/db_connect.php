<?php

class ProductDatabase extends mysqli
{
    public function __construct($servername, $username, $password, $dbName)
    {
        parent::__construct($servername, $username, $password, $dbName);
    }

    public function insertProduct($sku, $name, $price, $type, $special)
    {
        $queryString = "INSERT INTO products VALUES ('$sku', '$name', '$price', '$type', '$special')";
        parent::query($queryString);
    }

    public function deleteProduct($sku)
    {
        $queryString = "DELETE FROM products WHERE sku='$sku'";
        parent::query($queryString);
    }

    public function getProductEntries()
    {
        $queryString = "SELECT * FROM products ORDER BY type";
        return parent::query($queryString);
    }

    public function checkIfExists($sku)
    {
        $queryString = "SELECT * FROM products WHERE sku='$sku'";
        $queryResult = parent::query($queryString);

        if ($queryResult->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }
}

//Declare database connection variables
$serverName = "localhost";
$username = "root";
$password = "";
$dbName = "productDB";

$productDB = new ProductDatabase($serverName, $username, $password, $dbName);

//Check for connection errors
if ($productDB->connect_error){
    die("Failed to connect to database: " . $productDB->connect_error);
}

