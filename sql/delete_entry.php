<?php

include 'db_connect.php';

/**
 * Delete checked entries
 */

if (isset($_POST['delete_checkbox'])) {
    foreach ($_POST['delete_checkbox'] as $sku) {
        $productDB->deleteProduct($sku);
    }
}

$productDB->close();

header("Location: ../product/list.php");

