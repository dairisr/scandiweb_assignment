<?php

interface ProductAction
{
    public function returnSku();
    public function returnName();
    public function returnPrice();
}
interface TypeAction
{
    public function returnType();
    public function returnSpecial();
}
class Product implements ProductAction
{
    private $sku;
    private $name;
    private $price;

    public function __construct($sku, $name, $price)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
    }

    public function returnSku()
    {
        return $this->sku;
    }

    public function returnName()
    {
        return $this->name;
    }

    public function returnPrice()
    {
        return $this->price;
    }
}

class DvdDisc extends Product implements TypeAction
{
    private $size;

    function __construct($dvdSku, $dvdName, $dvdPrice, $dvdSize){
        parent::__construct($dvdSku, $dvdName, $dvdPrice);
        $this->size = $dvdSize;
    }

    public function returnType()
    {
        return 'dvd_disc';
    }

    public function returnSpecial()
    {
        return $this->size;
    }
}
class Book extends Product implements TypeAction
{
    private $weight;

    function __construct($bookSku, $bookName, $bookPrice, $bookWeight){
        parent::__construct($bookSku, $bookName, $bookPrice);
        $this->weight = $bookWeight;
    }

    public function returnType()
    {
        return 'book';
    }

    public function returnSpecial()
    {
        return $this->weight;
    }
}
class Furniture extends Product implements TypeAction
{
    private $height;
    private $width;
    private $length;

    function __construct($furnitureSku, $furnitureName, $furniturePrice, $furnitureHeight, $furnitureWidth, $furnitureLength)
    {
        parent::__construct($furnitureSku, $furnitureName, $furniturePrice);
        $this->height = $furnitureHeight;
        $this->width = $furnitureWidth;
        $this->length = $furnitureLength;
    }

    public function returnType()
    {
        return 'furniture';
    }

    public function returnSpecial()
    {
        return $this->height . 'x' . $this->width . 'x' . $this->length;
    }
}
