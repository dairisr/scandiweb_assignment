<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Product list</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>
        <div id="mainContainer">
            <div id="mainWrapper">
                <form action="../sql/delete_entry.php" method="post">
                    <div id="listNav">
                        <span class="h2">Product List</span>
                        <a href="new.php">Product add</a>
                        <input class="btn" type="submit" value="Delete">
                        <select class="form-control" id="deleteSelect" name="typeSelect">
                            <option value="" disabled selected hidden>Mass delete action</option>
                            <option value="mass_delete_all">Select all</option>
                        </select>
                    </div>
                    <hr>
                    <div id="entryWrapper">
                        <!-------------------------
                        <div class="entryBlock">
                        <input type="checkbox" name="delete_entry" value="TYPE">
                            <ul>
                                <li>SKU</li>
                                <li class="entryName">NAME</li>
                                <li>PRICE</li>
                                <li>DESCRIPTION</li>
                            </ul>
                        </div>
                        --------------------------->
                        <?php

                            include '../sql/db_connect.php';

                            // Get product entries
                            $queryResult = $productDB->getProductEntries();

                            //Insert HTML entry
                            if ($queryResult->num_rows > 0) {
                                while ($row = $queryResult->fetch_assoc()) {

                                    //Create association arrays for the entry to later use them in the HTML entry
                                    $typeTextAssocArray = array('dvd_disc' => 'Size: ', 'book' => 'Weight: ', 'furniture' => 'Dimension: ');
                                    $typeExtAssocArray = array('dvd_disc' => 'MB', 'book' => 'KG', 'furniture' => '');

                                    // Get unique text for row type
                                    $typeText = $typeTextAssocArray[$row['type']];
                                    $typeExt = $typeExtAssocArray[$row['type']];

                                    echo "
                                        <div class='entryBlock'>
                                        <input class='entryCheckBox' type='checkbox' name='delete_checkbox[]' value=" . $row['sku'] . ">
                                            <ul>
                                                <li>" . $row['sku'] . "</li>
                                                <li class='entryName'>" . $row['name'] . "</li>
                                                <li>" . $row['price'] . " $</li>
                                                <li>". $typeText . $row['type_desc'] . " " . $typeExt . "</li>
                                            </ul>
                                        </div>
                                    ";
                                }
                            } else {
                                //If there are no entries, send alert
                                echo "<div class='alert alert-info'>No entries in database</div>";
                            }
                            //Close connection
                            $productDB->close();
                        ?>
                    </div>
                    <script>
                        // Script for fade in animation
                        $().ready(function(){
                            $('#entryWrapper').hide();
                            $('#entryWrapper').fadeIn(300);
                        });
                        // Script for checking all checkboxes
                        $('#deleteSelect').change(function(){
                            if(this.value === "mass_delete_all"){
                                $('#entryWrapper :checkbox').each(function() {
                                    this.checked = true;
                                });
                            }
                        });
                    </script>
                </form>
            </div>
        </div>
    </body>
</html>
