<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Product add</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/error_functions.js"></script>
    </head>
    <body>
        <div id="mainContainer">
            <div id="mainWrapper">
                <form id="add_form" action="process_new.php" method="post">
                    <div id="listNav">
                    <span class="h2">Product Add</span>
                    <a href="list.php">Product list</a>
                    <input class="btn" type="submit" value="Save">
                    </div>
                    <hr>
                    <?php
                        if (isset($_SESSION['success'])) {
                            echo "<div class='alert alert-success'>Product successfuly added</div>";
                            session_destroy();
                        } else {
                            if(isset($_SESSION['error_empty_fields'])){
                                echo "<div id='error_empty_fields' class='alert alert-danger'>Please fill all fields</div>";
                            }
                            if(isset($_SESSION['error_sku_exists'])){
                                echo "<div id='error_sku_exists' class='alert alert-danger'>This SKU already exists</div>";
                            }
                            if(isset($_SESSION['error_no_type'])){
                                echo "<div id='error_no_type' class='alert alert-danger'>No type selected</div>";
                            }
                            if(isset($_SESSION['error_wrong_data'])){
                                echo "<div id='error_wrong_data' class='alert alert-danger'>Wrong data type in field</div>";
                            }
                            session_destroy();
                        }
                    ?>
                    <div class="addFields">
                        <label for="lblProductSku">SKU</label>
                        <input class="form-control" id="lblProductSku" type="text" name="productSku">
                    </div>
                    <script>
                        // Call function when is clicked away from input field
                        $('#lblProductSku').blur(function() {
                            glowEmptyFields(document.getElementById('lblProductSku'));
                        });
                    </script>
                    <br>
                    <div class="addFields">
                        <label for="lblProductName">Name</label>
                        <input class="form-control" id="lblProductName" type="text" name="productName">
                    </div>
                    <script>
                        $('#lblProductName').blur(function() {
                            glowEmptyFields(document.getElementById('lblProductName'));
                        });
                    </script>
                    <br>
                    <div class="addFields">
                        <label for="lblProductPrice">Price</label>
                        <input class="form-control" id="lblProductPrice" type="text" name="productPrice">
                    </div>
                    <script>
                        $('#lblProductPrice').blur(function() {
                            //First check if input is empty and then check if the type is correct
                            glowEmptyFields(document.getElementById('lblProductPrice'));
                            glowWrongType(document.getElementById('lblProductPrice'));
                        });
                    </script>
                    <br>
                    <div id="typeField">
                        <label for="typeSelect">Type Switcher</label>
                        <select class="form-control" id="typeSelect" name="typeSelect">
                            <option value="" disabled selected hidden>Type Switcher</option>
                            <option value="dvd_disc">DVD-disc</option>
                            <option value="book">Book</option>
                            <option value="furniture">Furniture</option>
                        </select>
                    </div>
                    <br>
                    <div id="typeDiv">
                        <div class="typeDisc" style="display:none">
                            <label for="lblProductDvdSize">Size</label>
                            <input class="form-control" id="lblProductDvdSize" type="text" name="productDvdSize">
                            <p>
                                Please provide DVD disc's size (mb)
                            </p>
                        </div>
                        <script>
                            $('#lblProductDvdSize').blur(function() {
                                glowEmptyFields(document.getElementById('lblProductDvdSize'));
                                glowWrongType(document.getElementById('lblProductDvdSize'));
                            });
                        </script>
                        <div class="typeBook" style="display:none">
                            <label for="lblProductBookWeight">Weight</label>
                            <input class="form-control" id="lblProductBookWeight" type="text" name="productBookWeight">
                            <p>
                                Please provide book's weight (kg)
                            </p>
                        </div>
                        <script>
                            $('#lblProductBookWeight').blur(function() {
                                glowEmptyFields(document.getElementById('lblProductBookWeight'));
                                glowWrongType(document.getElementById('lblProductBookWeight'));
                            });
                        </script>
                        <div class="typeFurniture" style="display:none">
                            <label for="lblProductFurnitureHeight">Height</label>
                            <input class="form-control" id="lblProductFurnitureHeight" type="text" name="productFurnitureHeight">
                            <br>
                            <label for="lblProductFurnitureWidth">Width</label>
                            <input class="form-control" id="lblProductFurnitureWidth" type="text" name="productFurnitureWidth">
                            <br>
                            <label for="lblProductFurnitureLength">Length</label>
                            <input class="form-control" id="lblProductFurnitureLength" type="text" name="productFurnitureLength">
                            <p>
                                Please provide height, width and length of the furniture (cm)
                            </p>
                        </div>
                        <script>
                            $('#lblProductFurnitureHeight').blur(function() {
                                glowEmptyFields(document.getElementById('lblProductFurnitureHeight'));
                                glowWrongType(document.getElementById('lblProductFurnitureHeight'));
                            });
                            $('#lblProductFurnitureWidth').blur(function() {
                                glowEmptyFields(document.getElementById('lblProductFurnitureWidth'));
                                glowWrongType(document.getElementById('lblProductFurnitureWidth'));
                            });
                            $('#lblProductFurnitureLength').blur(function() {
                                glowEmptyFields(document.getElementById('lblProductFurnitureLength'));
                                glowWrongType(document.getElementById('lblProductFurnitureLength'));
                            });
                        </script>
                    </div>
                </form>
            </div>
            <script>
                //Script of changing special attribute
                $('#typeSelect').change(function(){
                    $('#typeDiv').children().hide();
                    if (this.value === "dvd_disc") {
                        $('.typeDisc').show();
                    } else if (this.value === "book") {
                        $('.typeBook').show();
                    } else if (this.value === "furniture") {
                        $('.typeFurniture').show();
                    }
                });
            </script>
        </div>
    </body>
</html>
