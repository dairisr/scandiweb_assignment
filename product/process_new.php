<?php

session_start(); // Start the session to save ERRORs

include '../sql/db_connect.php'; // Database object
include 'product_type.php'; // Create product type objects

/**
 * Functions for error checking
 */

// Check if input is empty
function returnIfNotEmpty($string)
{
    if (empty($string)) {
        $_SESSION['error_empty_fields'] = true;
    }
    return;
}

// Check if input is correct data type
function returnIfWrongDT($string)
{
    if (!is_numeric($string)) {
        $_SESSION['error_wrong_data'] = true;
    }
    return;
}

/**
 * Getting values from form in new.php
 */

// Sku
$productSku = $productDB->real_escape_string(filter_input(INPUT_POST, 'productSku'));
// Name
$productName = $productDB->real_escape_string(filter_input(INPUT_POST, 'productName'));
// Price
$productPrice = $productDB->real_escape_string(filter_input(INPUT_POST, 'productPrice'));

// Type
$productType = $productDB->real_escape_string(filter_input(INPUT_POST, 'typeSelect'));

// DVD-Disc
$productDvdSize = $productDB->real_escape_string(filter_input(INPUT_POST, 'productDvdSize'));
// Book
$productBookWeight = $productDB->real_escape_string(filter_input(INPUT_POST, 'productBookWeight'));
// Furniture
$productFurnitureHeight = $productDB->real_escape_string(filter_input(INPUT_POST, 'productFurnitureHeight'));
$productFurnitureWidth = $productDB->real_escape_string(filter_input(INPUT_POST, 'productFurnitureWidth'));
$productFurnitureLength = $productDB->real_escape_string(filter_input(INPUT_POST, 'productFurnitureLength'));

/**
 * Error checking
 */

// Check if sku, name and price fields are empty
returnIfNotEmpty($productSku);
returnIfNotEmpty($productSku);
returnIfNotEmpty($productSku);

// Check if sku exists
if ($productDB->checkIfExists($productSku)) {
    $_SESSION['error_sku_exists'] = true;
}

// Check if price is not numeric
returnIfWrongDT($productPrice);

// Check if special attributes are empty, type is number or no type is selected
if ($productType === 'dvd_disc') {
    returnIfNotEmpty($productDvdSize);
    returnIfWrongDT($productDvdSize);
} else if ($productType === 'book') {
    returnIfNotEmpty($productBookWeight);
    returnIfWrongDT($productBookWeight);
} else if ($productType === 'furniture') {
    returnIfNotEmpty($productFurnitureHeight);
    returnIfWrongDT($productFurnitureHeight);
    returnIfNotEmpty($productFurnitureWidth);
    returnIfWrongDT($productFurnitureWidth);
    returnIfNotEmpty($productFurnitureLength);
    returnIfWrongDT($productFurnitureLength);
}  else {
    $_SESSION['error_no_type'] = true;
}

/**
 * Product object creation if no error is found
 */

if (! isset($_SESSION['error_sku_exists']) && ! isset($_SESSION['error_empty_fields']) && ! isset($_SESSION['error_wrong_data']) && ! isset($_SESSION['error_no_type'])) {
    // Object creation
    if ($productType === 'dvd_disc') {
        $product = new DvdDisc($productSku, $productName, $productPrice, $productDvdSize);
    } else if ($productType === 'book') {
        $product = new Book($productSku, $productName, $productPrice, $productBookWeight);
    } else if ($productType === 'furniture') {
        $product = new Furniture($productSku, $productName, $productPrice, $productFurnitureHeight, $productFurnitureWidth, $productFurnitureLength);
    }

    // Insert object values in database
    $productDB->insertProduct($product->returnSku(), $product->returnName(), $product->returnPrice(), $product->returnType(),$product->returnSpecial());
    // Success message
    $_SESSION['success'] = true;
}

////Close DB connection
$productDB->close();

header('Location: new.php');