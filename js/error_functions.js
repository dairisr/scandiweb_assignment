function glowEmptyFields(field)
{
    // Add glow if field is empty
    if (field.value == '' || field.value == null && !field.classList.contains('redGlow')) {
        field.classList.add('redGlow');
        field.placeholder = 'Fill this field';
    } else {
        field.classList.remove('redGlow');
    }
}
function glowWrongType(field)
{
    // Add glow to field if it isn't numeric
    if (isNaN(field.value) && !field.classList.contains('redGlow')) {
        field.className += ' redGlow';
        alert('Only numbers allowed');
    }
}